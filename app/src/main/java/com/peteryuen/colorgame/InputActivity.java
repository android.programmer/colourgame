package com.peteryuen.colorgame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.peteryuen.colorgame.database.DatabaseHelper;
import com.peteryuen.colorgame.database.Person;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class InputActivity extends Activity {

    private DatabaseHelper mDatabaseHelper;
    private Button mButtonOk;
    private TextView mTextViewScore;
    private int mScore, mRanking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);
        init();
    }

    private void init() {
        final Person p = new Person();
        final Intent intent = getIntent();
        short defVal = 0;
        mScore = intent.getShortExtra("score", defVal);
        p.setScore(mScore);
        mDatabaseHelper = new DatabaseHelper(this, "colour_memory.db3", 1);
        mTextViewScore = findViewById(R.id.field_text_score);
        String strScore = String.valueOf(mScore);
        mTextViewScore.setText(strScore);
        mButtonOk = findViewById(R.id.button_ok);
        mButtonOk.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String strName = ((EditText) findViewById(R.id.field_edit_name)).getText().toString();
                if (strName.length() > 0) {
                    p.setName(strName);
                    insertData(p);
                } else
                    Toast.makeText(InputActivity.this, "Please enter your name.", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void insertData(Person p) {
        new InsertTask(mDatabaseHelper).execute(p);
    }

    class InsertTask extends AsyncTask<Person, Integer, Void> {
        private DatabaseHelper mDatabaseHelper;
        private List<Map<String, Object>> mQueryList;
        long start;

        public InsertTask(DatabaseHelper dbHelper) {
            this.mDatabaseHelper = dbHelper;
        }

        @Override
        protected Void doInBackground(Person... params) {
            start = System.currentTimeMillis();
            Person p = params[0];
            List<Person> list = new ArrayList<>();
            list.add(p);
            mDatabaseHelper.insertData(list);
            //1 get all the database data
            mQueryList = mDatabaseHelper.queryAll();
            //2 convert map to person list
            List<Person> persons = mDatabaseHelper.convertMapToPersonList(mQueryList);

            //3. sort
            Collections.sort(persons);

            //4 reset ranking
            persons = mDatabaseHelper.resetRanking(persons);
            //5 update database
            mDatabaseHelper.updateData(persons);

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("insertTask", "insertTAsk time needed:" + (System.currentTimeMillis() - start));
            if (mDatabaseHelper != null)
                mDatabaseHelper.close();
            AlertDialog.Builder builder = new Builder(InputActivity.this);
            mRanking = mDatabaseHelper.getRanking(mScore);
            builder.setTitle("Thank you for playing this game! Your ranking is " + mRanking + ".  Let's play again!");
            builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    goodBye();
                }
            });
            builder.setOnCancelListener(new DialogInterface.OnCancelListener() {

                @Override
                public void onCancel(DialogInterface dialog) {
                    goodBye();
                }
            });
            builder.create().show();
        }
    }

    private void goodBye() {
        int pid = android.os.Process.myPid();
        android.os.Process.killProcess(pid);
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mDatabaseHelper != null)
            mDatabaseHelper.close();
    }
}
