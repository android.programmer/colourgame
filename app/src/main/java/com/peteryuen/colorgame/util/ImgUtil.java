package com.peteryuen.colorgame.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.peteryuen.colorgame.R;
import com.peteryuen.colorgame.view.CardImage;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class ImgUtil {

    //Original list with 8 id
    private static List<Integer> sImgId = getImgId();

    public static List<Integer> getImgId() {

        Field[] fields = R.drawable.class.getFields();
        List<Integer> imgId = new ArrayList<>();

        for (Field f : fields) {
            if (f.getName().startsWith("colour")) {
                try {
                    imgId.add(f.getInt(R.drawable.class));
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return imgId;
    }

    public static List<Integer> getRandomId(List<Integer> sourceId, int size) {
        Random random = new Random();
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            int index = random.nextInt(sourceId.size());
            Integer id = sourceId.get(index);
            result.add(id);
        }
        return result;
    }

    public static List<Integer> getCardImageId(int size) {
        List<Integer> cardImageId = getRandomId(sImgId, size);
        cardImageId.addAll(cardImageId);
        Collections.shuffle(cardImageId);

        return cardImageId;
    }


    public static List<CardImage> getCardImages(Context context, int size) {
        List<Integer> cardList = getCardImageId(size);
        List<CardImage> result = new ArrayList<>();

        for (Integer id : cardList) {
            Bitmap cardBitmap = BitmapFactory.decodeResource(context.getResources(), id);
            Bitmap backgroundBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.card_bg);
            CardImage ci = new CardImage(id, cardBitmap, backgroundBitmap, context);
            result.add(ci);
        }
        return result;
    }

    public static Bitmap getSelectImage(Context context) {
        return BitmapFactory.decodeResource(context.getResources(),
                R.drawable.selected);
    }
}
