package com.peteryuen.colorgame;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.peteryuen.colorgame.util.ImgUtil;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

@TargetApi(11)
public class GameActivity extends Activity {

    protected static final int FAILURE_MATCH = 0;
    protected static final int SUCCESS_MATCH = 2;
    private TextView mTextViewCurScore;
    private Button mBtnHighScore;
    private LinearLayout mLinearLayout;
    private List<Integer> mImageListId;
    //    Map that stores colorCardId <-> backgroundCardId, it is used during failure attempt on matching the cards, then the cards have to reset to background card
    private Map<Integer, Integer> mColorToBackgroundMap;

    private boolean mIsFinished;
    private byte mSuccessCount;
    private short mScore = 0;
    private ImageView mSelectedImage;
    private ImageView mCurrentImage;
    public Handler handler;

    private ImageView mImageViewColorCard1;
    private ImageView mImageViewColorCard2;
    private ImageView mImageViewColorCard3;
    private ImageView mImageViewColorCard4;
    private ImageView mImageViewColorCard5;
    private ImageView mImageViewColorCard6;
    private ImageView mImageViewColorCard7;
    private ImageView mImageViewColorCard8;
    private ImageView mImageViewColorCard9;
    private ImageView mImageViewColorCard10;
    private ImageView mImageViewColorCard11;
    private ImageView mImageViewColorCard12;
    private ImageView mImageViewColorCard13;
    private ImageView mImageViewColorCard14;
    private ImageView mImageViewColorCard15;
    private ImageView mImageViewColorCard16;

    private ImageView mImageViewBackgroundCard1;
    private ImageView mImageViewBackgroundCard2;
    private ImageView mImageViewBackgroundCard3;
    private ImageView mImageViewBackgroundCard4;
    private ImageView mImageViewBackgroundCard5;
    private ImageView mImageViewBackgroundCard6;
    private ImageView mImageViewBackgroundCard7;
    private ImageView mImageViewBackgroundCard8;
    private ImageView mImageViewBackgroundCard9;
    private ImageView mImageViewBackgroundCard10;
    private ImageView mImageViewBackgroundCard11;
    private ImageView mImageViewBackgroundCard12;
    private ImageView mImageViewBackgroundCard13;
    private ImageView mImageViewBackgroundCard14;
    private ImageView mImageViewBackgroundCard15;
    private ImageView mImageViewBackgroundCard16;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    @SuppressLint({"NewApi", "NewApi"})
    private void init() {
        mTextViewCurScore = findViewById(R.id.text_current_score);
        mBtnHighScore = findViewById(R.id.button_high_score);
        mLinearLayout = findViewById(R.id.linear_0);
        mImageListId = ImgUtil.getCardImageId(Objects.requireNonNull(ImgUtil.getImgId()).size());

        mImageViewColorCard1 = findViewById(R.id.image_color_card_1);
        mImageViewColorCard2 = findViewById(R.id.image_color_card_2);
        mImageViewColorCard3 = findViewById(R.id.image_color_card_3);
        mImageViewColorCard4 = findViewById(R.id.image_color_card_4);
        mImageViewColorCard5 = findViewById(R.id.image_color_card_5);
        mImageViewColorCard6 = findViewById(R.id.image_color_card_6);
        mImageViewColorCard7 = findViewById(R.id.image_color_card_7);
        mImageViewColorCard8 = findViewById(R.id.image_color_card_8);
        mImageViewColorCard9 = findViewById(R.id.image_color_card_9);
        mImageViewColorCard10 = findViewById(R.id.image_color_card_10);
        mImageViewColorCard11 = findViewById(R.id.image_color_card_11);
        mImageViewColorCard12 = findViewById(R.id.image_color_card_12);
        mImageViewColorCard13 = findViewById(R.id.image_color_card_13);
        mImageViewColorCard14 = findViewById(R.id.image_color_card_14);
        mImageViewColorCard15 = findViewById(R.id.image_color_card_15);
        mImageViewColorCard16 = findViewById(R.id.image_color_card_16);

        mImageViewColorCard1.setImageResource(mImageListId.get(1));
        mImageViewColorCard2.setImageResource(mImageListId.get(2));
        mImageViewColorCard3.setImageResource(mImageListId.get(3));
        mImageViewColorCard4.setImageResource(mImageListId.get(4));
        mImageViewColorCard5.setImageResource(mImageListId.get(5));
        mImageViewColorCard6.setImageResource(mImageListId.get(6));
        mImageViewColorCard7.setImageResource(mImageListId.get(7));
        mImageViewColorCard8.setImageResource(mImageListId.get(8));
        mImageViewColorCard9.setImageResource(mImageListId.get(9));
        mImageViewColorCard10.setImageResource(mImageListId.get(10));
        mImageViewColorCard11.setImageResource(mImageListId.get(11));
        mImageViewColorCard12.setImageResource(mImageListId.get(12));
        mImageViewColorCard13.setImageResource(mImageListId.get(13));
        mImageViewColorCard14.setImageResource(mImageListId.get(14));
        mImageViewColorCard15.setImageResource(mImageListId.get(15));
        mImageViewColorCard16.setImageResource(mImageListId.get(0));
        mImageViewColorCard1.setTag(mImageListId.get(1));
        mImageViewColorCard2.setTag(mImageListId.get(2));
        mImageViewColorCard3.setTag(mImageListId.get(3));
        mImageViewColorCard4.setTag(mImageListId.get(4));
        mImageViewColorCard5.setTag(mImageListId.get(5));
        mImageViewColorCard6.setTag(mImageListId.get(6));
        mImageViewColorCard7.setTag(mImageListId.get(7));
        mImageViewColorCard8.setTag(mImageListId.get(8));
        mImageViewColorCard9.setTag(mImageListId.get(9));
        mImageViewColorCard10.setTag(mImageListId.get(10));
        mImageViewColorCard11.setTag(mImageListId.get(11));
        mImageViewColorCard12.setTag(mImageListId.get(12));
        mImageViewColorCard13.setTag(mImageListId.get(13));
        mImageViewColorCard14.setTag(mImageListId.get(14));
        mImageViewColorCard15.setTag(mImageListId.get(15));
        mImageViewColorCard16.setTag(mImageListId.get(0));

        mImageViewBackgroundCard1 = findViewById(R.id.image_background_card_1);
        mImageViewBackgroundCard2 = findViewById(R.id.image_background_card_2);
        mImageViewBackgroundCard3 = findViewById(R.id.image_background_card_3);
        mImageViewBackgroundCard4 = findViewById(R.id.image_background_card_4);
        mImageViewBackgroundCard5 = findViewById(R.id.image_background_card_5);
        mImageViewBackgroundCard6 = findViewById(R.id.image_background_card_6);
        mImageViewBackgroundCard7 = findViewById(R.id.image_background_card_7);
        mImageViewBackgroundCard8 = findViewById(R.id.image_background_card_8);
        mImageViewBackgroundCard9 = findViewById(R.id.image_background_card_9);
        mImageViewBackgroundCard10 = findViewById(R.id.image_background_card_10);
        mImageViewBackgroundCard11 = findViewById(R.id.image_background_card_11);
        mImageViewBackgroundCard12 = findViewById(R.id.image_background_card_12);
        mImageViewBackgroundCard13 = findViewById(R.id.image_background_card_13);
        mImageViewBackgroundCard14 = findViewById(R.id.image_background_card_14);
        mImageViewBackgroundCard15 = findViewById(R.id.image_background_card_15);
        mImageViewBackgroundCard16 = findViewById(R.id.image_background_card_16);

// initialize mapping between color card and background card
        mColorToBackgroundMap = new HashMap<>();
        mColorToBackgroundMap.put(mImageViewColorCard1.getId(), mImageViewBackgroundCard1.getId());
        mColorToBackgroundMap.put(mImageViewColorCard2.getId(), mImageViewBackgroundCard2.getId());
        mColorToBackgroundMap.put(mImageViewColorCard3.getId(), mImageViewBackgroundCard3.getId());
        mColorToBackgroundMap.put(mImageViewColorCard4.getId(), mImageViewBackgroundCard4.getId());
        mColorToBackgroundMap.put(mImageViewColorCard5.getId(), mImageViewBackgroundCard5.getId());
        mColorToBackgroundMap.put(mImageViewColorCard6.getId(), mImageViewBackgroundCard6.getId());
        mColorToBackgroundMap.put(mImageViewColorCard7.getId(), mImageViewBackgroundCard7.getId());
        mColorToBackgroundMap.put(mImageViewColorCard8.getId(), mImageViewBackgroundCard8.getId());
        mColorToBackgroundMap.put(mImageViewColorCard9.getId(), mImageViewBackgroundCard9.getId());
        mColorToBackgroundMap.put(mImageViewColorCard10.getId(), mImageViewBackgroundCard10.getId());
        mColorToBackgroundMap.put(mImageViewColorCard11.getId(), mImageViewBackgroundCard11.getId());
        mColorToBackgroundMap.put(mImageViewColorCard12.getId(), mImageViewBackgroundCard12.getId());
        mColorToBackgroundMap.put(mImageViewColorCard13.getId(), mImageViewBackgroundCard13.getId());
        mColorToBackgroundMap.put(mImageViewColorCard14.getId(), mImageViewBackgroundCard14.getId());
        mColorToBackgroundMap.put(mImageViewColorCard15.getId(), mImageViewBackgroundCard15.getId());
        mColorToBackgroundMap.put(mImageViewColorCard16.getId(), mImageViewBackgroundCard16.getId());

//set onClickListener for every view.
        setOnClickListener(mImageViewColorCard1);
        setOnClickListener(mImageViewBackgroundCard1, mImageViewColorCard1);
        setOnClickListener(mImageViewColorCard2);
        setOnClickListener(mImageViewBackgroundCard2, mImageViewColorCard2);
        setOnClickListener(mImageViewColorCard3);
        setOnClickListener(mImageViewBackgroundCard3, mImageViewColorCard3);
        setOnClickListener(mImageViewColorCard4);
        setOnClickListener(mImageViewBackgroundCard4, mImageViewColorCard4);
        setOnClickListener(mImageViewColorCard5);
        setOnClickListener(mImageViewBackgroundCard5, mImageViewColorCard5);
        setOnClickListener(mImageViewColorCard6);
        setOnClickListener(mImageViewBackgroundCard6, mImageViewColorCard6);
        setOnClickListener(mImageViewColorCard7);
        setOnClickListener(mImageViewBackgroundCard7, mImageViewColorCard7);
        setOnClickListener(mImageViewColorCard8);
        setOnClickListener(mImageViewBackgroundCard8, mImageViewColorCard8);
        setOnClickListener(mImageViewColorCard9);
        setOnClickListener(mImageViewBackgroundCard9, mImageViewColorCard9);
        setOnClickListener(mImageViewColorCard10);
        setOnClickListener(mImageViewBackgroundCard10, mImageViewColorCard10);
        setOnClickListener(mImageViewColorCard11);
        setOnClickListener(mImageViewBackgroundCard11, mImageViewColorCard11);
        setOnClickListener(mImageViewColorCard12);
        setOnClickListener(mImageViewBackgroundCard12, mImageViewColorCard12);
        setOnClickListener(mImageViewColorCard13);
        setOnClickListener(mImageViewBackgroundCard13, mImageViewColorCard13);
        setOnClickListener(mImageViewColorCard14);
        setOnClickListener(mImageViewBackgroundCard14, mImageViewColorCard14);
        setOnClickListener(mImageViewColorCard15);
        setOnClickListener(mImageViewBackgroundCard15, mImageViewColorCard15);
        setOnClickListener(mImageViewColorCard16);
        setOnClickListener(mImageViewBackgroundCard16, mImageViewColorCard16);

        mBtnHighScore.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GameActivity.this, HighScoreActivity.class);
                startActivity(intent);
            }
        });

        handler = new Handler() {

            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case FAILURE_MATCH:
                        mScore -= 1;
                        mTextViewCurScore.setText("Score:" + mScore);
                        failureFlip(mSelectedImage, mCurrentImage);
                        mSelectedImage = null;
                        mCurrentImage = null;
                        break;

                    case SUCCESS_MATCH:
                        if (mSelectedImage == null || mCurrentImage == null)
                            return;
                        mSelectedImage.setVisibility(View.INVISIBLE);
                        mCurrentImage.setVisibility(View.INVISIBLE);
                        mTextViewCurScore.setText("Score:" + mScore);
                        mSelectedImage = null;
                        mCurrentImage = null;
                        mLinearLayout.setEnabled(true);
                        if (mSuccessCount > 7)
                            mIsFinished = true;
                        if (mIsFinished) {
                            mSuccessCount = 0;
                            createFinishedDialog();
                        }
                        break;
                }
            }
        };
    }

    private void setOnClickListener(final ImageView bmp) {
        bmp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                bmp.setEnabled(false);
                animateCardFlip(bmp);
            }
        });
    }

    private void setOnClickListener(final ImageView imageViewBackgroundCard, final ImageView imageViewColorCard) {
        imageViewBackgroundCard.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!imageViewColorCard.isEnabled())
                    return;
                if (!mLinearLayout.isEnabled())
                    return;
                imageViewColorCard.setEnabled(false);
                final ImageView visibleList;
                final ImageView imageViewInvisibleList;

                if (imageViewBackgroundCard.getVisibility() == View.GONE) {
                    visibleList = imageViewColorCard;
                    imageViewInvisibleList = imageViewBackgroundCard;
                } else {
                    imageViewInvisibleList = imageViewColorCard;
                    visibleList = imageViewBackgroundCard;
                }

                ObjectAnimator visToInvis = ObjectAnimator.ofFloat(visibleList, "rotationY", 0f, 90f);
                visToInvis.setDuration(500);
                final ObjectAnimator imageViewInvisToVis = ObjectAnimator.ofFloat(imageViewInvisibleList, "rotationY",
                        -90f, 0f);
                imageViewInvisToVis.setDuration(500);
                visToInvis.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator anim) {
                        visibleList.setVisibility(View.GONE);
                        imageViewInvisToVis.start();
                        imageViewInvisibleList.setVisibility(View.VISIBLE);
                    }
                });
                visToInvis.start();
                decideSuccessOrFailure(imageViewColorCard);
            }
        });
    }

    private boolean isSame(ImageView currentImage, ImageView selectedImage) {
        return currentImage.getTag() == selectedImage.getTag();
    }

    private void createFinishedDialog() {
        AlertDialog.Builder builder = new Builder(GameActivity.this);
        builder.setTitle("You have just finished the game!");
        builder.setOnCancelListener(new OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
                Intent intent = new Intent(GameActivity.this, InputActivity.class);
                intent.putExtra("score", mScore);
                startActivity(intent);
            }
        });
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(GameActivity.this, InputActivity.class);
                intent.putExtra("score", mScore);
                startActivity(intent);
            }
        });
        builder.create().show();
    }

    private void handleSuccess() {
        mSuccessCount++;
        mScore += 2;
        handler.sendEmptyMessageDelayed(SUCCESS_MATCH, 1000);
    }

    private void handleFailure(ImageView currentImage, ImageView selectedImage) {
        handler.sendEmptyMessageDelayed(FAILURE_MATCH, 1000);
    }

    private void failureFlip(ImageView iamgeViewSelectedImage, ImageView imageViewCurrentImage) {
        animateCardFlip(iamgeViewSelectedImage);
        animateCardFlip(imageViewCurrentImage);
    }

    private void animateCardFlip(final ImageView imageView) {
        int correspondingBackgroundCardId = getCorrespondingBackgroundCardId(imageView);
        ImageView backgroundBitmap = findViewById(correspondingBackgroundCardId);

        final ImageView imageViewVisibleList;
        final ImageView imageViewInvisibleList;
        int visibility = backgroundBitmap.getVisibility();

        if (visibility == View.GONE) {
            imageViewVisibleList = imageView;
            imageViewInvisibleList = backgroundBitmap;
        } else {
            imageViewInvisibleList = imageView;
            imageViewVisibleList = backgroundBitmap;
        }

        ObjectAnimator visToInvis = ObjectAnimator.ofFloat(imageViewVisibleList, "rotationY", 0f, 90f);
        visToInvis.setDuration(500);
        final ObjectAnimator invisToVis = ObjectAnimator.ofFloat(imageViewInvisibleList, "rotationY",
                -90f, 0f);
        invisToVis.setDuration(500);
        visToInvis.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator anim) {
                imageViewVisibleList.setVisibility(View.GONE);
                invisToVis.start();
                imageViewInvisibleList.setVisibility(View.VISIBLE);
            }
        });
        visToInvis.start();
        invisToVis.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator anim) {
                mLinearLayout.setEnabled(true);
                imageView.setEnabled(true);
            }
        });
    }

    private void decideSuccessOrFailure(ImageView imageView) {
        mCurrentImage = imageView;
        Log.i("GameActivity", "currentImage:" + mCurrentImage.getTag());
        if (mSelectedImage != null)
            Log.i("GameActivity", "selectedImage 1:" + mSelectedImage.getTag());

        if (mSelectedImage != null) {

            mLinearLayout.setEnabled(false);
            if (isSame(mCurrentImage, mSelectedImage) && mCurrentImage.getId() != mSelectedImage.getId()) {
                handleSuccess();
            } else {
                handleFailure(mCurrentImage, mSelectedImage);
            }
        } else {
            mSelectedImage = mCurrentImage;
            Log.i("GameActivity", "selectedImage 4:" + mSelectedImage.getTag());
        }
    }

    private int getCorrespondingBackgroundCardId(ImageView imageViewColorCard) {
        return mColorToBackgroundMap.get(imageViewColorCard.getId());
    }
}
