package com.peteryuen.colorgame.view;

import com.peteryuen.colorgame.R;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

@TargetApi(12)
public class CardImage {
	private int imageId;
	private int bgImageId;
	private ImageView iv;
	
	private Bitmap bm;
	private Bitmap bgBm;
	
	private boolean filpped;
	
	public CardImage(int imageId,Bitmap bm,Bitmap bgBm,Context context)
	{
		this.imageId=imageId;
		this.bgImageId=R.drawable.card_bg;
		this.bm=bm;
		this.bgBm = bgBm;
		this.iv= new ImageView(context);
		this.iv.setImageBitmap(bgBm);

		//Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.card_bg);
		//Log.i("CardImage","bitmap:"+bitmap);
	}
	public ImageView getIv() {
		return iv;
	}
	
	public void setIv(ImageView iv) {
		this.iv = iv;
	}

	public int getImageId() {
		return imageId;
	}

	public void setImageId(int imageId) {
		this.imageId = imageId;
	}

	public Bitmap getBm() {
		return bm;
	}

	public void setBm(Bitmap bm) {
		this.bm = bm;
	}

	public Bitmap getBgBm() {
		return bgBm;
	}

	public void setBgBm(Bitmap bgBm) {
		this.bgBm = bgBm;
	}
	public boolean isFilpped() {
		return filpped;
	}
	public void setFilpped(boolean filpped) {
		this.filpped = filpped;
	}
	public int getBgImageId() {
		return bgImageId;
	}
	public void setBgImageId(int bgImageId) {
		this.bgImageId = bgImageId;
	}
	
}
