package com.peteryuen.colorgame;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.peteryuen.colorgame.database.DatabaseHelper;
import com.peteryuen.colorgame.database.Person;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class HighScoreActivity extends Activity {

    private ListView mList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_highscore);
        init();
    }

    private void init() {
        new QueryTask().execute();
        mList = findViewById(R.id.list_highscore);
    }

    class QueryTask extends AsyncTask<Void, Void, List<Map<String, Object>>> {

        @Override
        protected List<Map<String, Object>> doInBackground(Void... params) {

            DatabaseHelper dbHelper = new DatabaseHelper(HighScoreActivity.this, "colour_memory.db3", 1);
//				SQLiteDatabase db =dbHelper.getWritableDatabase();
//				db.delete(DatabaseHelper.TABLE, null, null);
            List<Map<String, Object>> listItems = dbHelper.queryAll();
            List<Person> persons = dbHelper.convertMapToPersonList(listItems);
            Collections.sort(persons);

            listItems.clear();

            for (Person p : persons) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("ranking", p.getRanking());
                map.put("name", p.getName());
                map.put("score", p.getScore());
                listItems.add(map);
            }
            return listItems;
        }

        @Override
        protected void onPostExecute(List<Map<String, Object>> result) {
            SimpleAdapter adapter = new SimpleAdapter(HighScoreActivity.this
                    , result
                    , R.layout.list_highscore
                    , new String[]{"ranking", "name", "score"}
                    , new int[]{R.id.text_rank, R.id.text_name, R.id.text_score});
            mList.setAdapter(adapter);
            super.onPostExecute(result);
        }
    }
}