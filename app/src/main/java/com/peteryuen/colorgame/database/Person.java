package com.peteryuen.colorgame.database;

public class Person implements Comparable<Person> {

    private int mId;
    private int mRanking;
    private String mName;
    private int mScore;

    public int getId() {
        return mId;
    }

    public void setId(int mid) {
        this.mId = mid;
    }

    public int getRanking() {
        return mRanking;
    }

    public void setRanking(int mRanking) {
        this.mRanking = mRanking;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public int getScore() {
        return mScore;
    }

    public void setScore(int mScore) {
        this.mScore = mScore;
    }

    public int compareTo(Person otherP) {
        return otherP.getScore() - this.getScore();
    }

    public String toString() {
        return "_id:" + mId + ", Ranking:" + getRanking() + " ,name:" + getName() + " ,score:" + getScore();
    }
}
