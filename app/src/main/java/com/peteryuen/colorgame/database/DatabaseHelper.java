package com.peteryuen.colorgame.database;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static String TABLE = "colour_memory";
    final private String CREATE_TABLE = "create table if not exists " + TABLE + " (_id integer primary key autoincrement, name text not null,ranking ,score )";

    public DatabaseHelper(Context context, String name, int version) {
        super(context, name, null, version);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public int getRanking(int score) {
        List<Map<String, Object>> queryList = queryAll();
        List<Person> persons = convertMapToPersonList(queryList);
        Collections.sort(persons);

        for (Person p : persons) {
            if (score == p.getScore()) {
                return p.getRanking();
            }
        }
        return 0;
    }

    public void insertData(List<Person> persons) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        SQLiteStatement stmt = db.compileStatement(
                "INSERT INTO " + DatabaseHelper.TABLE + " VALUES (?,?,?,?)");
        for (Person p : persons) {
            stmt.bindNull(1);
            stmt.bindString(2, p.getName());
            stmt.bindLong(3, p.getRanking());
            stmt.bindLong(4, p.getScore());
            stmt.executeInsert();
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public List<Map<String, Object>> queryAll() {
        long start = System.currentTimeMillis();
        SQLiteDatabase db = this.getReadableDatabase();
        db.beginTransaction();
        Cursor c = db.query(DatabaseHelper.TABLE, null, null, null, null, null, null);

        List<Map<String, Object>> listItems = new ArrayList<Map<String, Object>>();
        Log.i("Database", "query all size:" + c.getCount());
        if (c.moveToFirst()) {
            do {
                Map<String, Object> listItem = new TreeMap<String, Object>();
                String _id = c.getString(0);
                String name = c.getString(1);
                String ranking = c.getString(2);
                String score = c.getString(3);
                listItem.put("_id", _id);
                listItem.put("name", name);
                listItem.put("ranking", ranking);
                listItem.put("score", score);
                listItems.add(listItem);
                Log.i("Database", "_id:" + _id + " ,name:" + name + " ,ranking:" + ranking + " , score:" + score);
            } while (c.moveToNext());
        }
        c.close();
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
        Log.i("databasehelper", "queryall time:" + (System.currentTimeMillis() - start));
        return listItems;
    }

    public List<Person> convertMapToPersonList(List<Map<String, Object>> queryList) {
        List<Person> persons = new ArrayList<Person>();
        for (Map<String, Object> l : queryList) {
            Person person = new Person();
            int temp_id = Integer.valueOf((String) l.get("_id"));
            person.setId(temp_id);
            person.setName((String) l.get("name"));
            int tempRanking = Integer.valueOf((String) l.get("ranking"));
            person.setRanking(tempRanking);
            int tempScore = Integer.valueOf((String) l.get("score"));
            person.setScore(tempScore);
            persons.add(person);
        }
        return persons;
    }

    public List<Person> resetRanking(List<Person> persons) {
        int len = persons.size();
        List<Person> ranking = persons;
        ranking.get(0).setRanking(1);
        for (int i = 1; i < len; i++) {
            int rank = persons.get(i).getScore() == persons.get(i - 1).getScore() ? persons.get(i - 1).getRanking() : i + 1;
            ranking.get(i).setRanking(rank);
        }
        return ranking;
    }

    public void updateData(List<Person> persons) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        SQLiteStatement stmt = db.compileStatement(
                "UPDATE " + TABLE + " SET ranking = ?,name = ?, score=? WHERE _id = ?");

        for (Person person : persons) {
            stmt.clearBindings();
            stmt.bindLong(1, person.getRanking());
            stmt.bindString(2, person.getName());
            stmt.bindLong(3, person.getScore());
            stmt.bindLong(4, person.getId());
            stmt.executeUpdateDelete();
            Log.i("update data", "" + person);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

}
