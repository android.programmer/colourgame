# Color Game

This project was done about 7 years ago before I knew how to use git which is why the initial
commit is a huge one, some of the code is deprecated and the format of layout may be outdated.

The **Android** game board consists of a 4x4 grid with 8 pairs of color cards.
The game starts initially with all cards facing down. The player is to then flip two cards each
round, trying to find a match. If the flipped pair is a match, the player receives 2 points,
and the cards may be removed from the game board. Otherwise, the cards are turned face-
down again and the player loses 1 point. This continues until all pairs have been found.

<img src="./main.jpg" width="400">
<img src="./ranking.jpg" width="400">
